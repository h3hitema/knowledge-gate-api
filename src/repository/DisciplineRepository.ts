import {getRepository} from "typeorm";
import {Discipline} from "../entity/Discipline";
import {Application} from "../entity/Application";
import {User} from "../entity/User";

class DisciplineRepository {
    static getById = async (id: number) => {
        const disciplineRepository = getRepository(Discipline);
        return await disciplineRepository.findOneOrFail(id);
    };

    static getAll = async () => {
        const disciplineRepository = getRepository(Discipline);
        return await disciplineRepository
            .createQueryBuilder("discipline")
            .getMany();
    };

    static getDisciplineExams = async (id: number) => {
        const disciplineRepository = getRepository(Discipline);
        return await disciplineRepository
            .createQueryBuilder('discipline')
            .innerJoin('discipline.exams', 'exam')
            .where('discipline.id = :id', {id: id})
            .getOne();
    };

    // static getDisciplineWithTeachers = async (id: number) => {
    //     const disciplineRepository = getRepository(Discipline);
    //     return await disciplineRepository
    //         .createQueryBuilder('discipline')
    //         .innerJoinAndSelect('discipline.teachers', 'teacher')
    //         .where('discipline.id = :id', {id: id})
    //         .getOne();
    // };
    static getDisciplineWithTeachers = async (id: number) => {
        const disciplineRepository = getRepository(Discipline);
        return await disciplineRepository.findOneOrFail({
            relations: ["teachers"],
            where: [{id: id}]
        });
    };

    static getDisciplineWithRelations= async (id: number) => {
        const disciplineRepository = getRepository(Discipline);
        return await disciplineRepository.findOneOrFail({
            relations: ["teachers","students"],
            where: [{id: id}]
        });
    };

    static getDisciplineWithStudents = async (id: number) => {
        const disciplineRepository = getRepository(Discipline);
        return await disciplineRepository
            .createQueryBuilder('discipline')
            .innerJoin('discipline.students', 'student')
            .where('discipline.id = :id', {id: id})
            .andWhere('student.role = :role', {role: "STUDENT"})
            .getOne();
    };
}

export default DisciplineRepository;
