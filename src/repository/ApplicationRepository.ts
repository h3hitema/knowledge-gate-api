import {getRepository} from "typeorm";
import {Discipline} from "../entity/Discipline";
import {Application} from "../entity/Application";

class ApplicationRepository {
    static getById = async (id: number) => {
        const applicationRepository = getRepository(Application);
        return await applicationRepository.findOneOrFail(id);
    };

    static getUserDisciplineApplication = async (student_id: number, discipline_id) => {
        const applicationRepository = getRepository(Application);
        return await applicationRepository
            .createQueryBuilder('application')
            .innerJoin('application.student', 'student')
            .where('student.id = :student_id', {student_id: student_id})
            .andWhere('application.discipline_id = :discipline_id', {discipline_id: discipline_id})
            .getOne();
    };

    static getDisciplineApplications = async (id: number) => {
        const applicationRepository = getRepository(Application);
        return await applicationRepository
            .createQueryBuilder('application')
            .innerJoin("application.discipline", "discipline")
            .innerJoin('application.student', 'student')
            .where('discipline.id = :id', {id: id})
            .andWhere('student.deleted = :deleted', {deleted: true})
            .getMany();
    };

    static getApplicationWithRelations = async (id: number) => {
        const applicationRepository = getRepository(Application);
        return await applicationRepository.findOneOrFail({
            relations: ["discipline","student"],
            where: [{id: id}]
        });
    };

    static getDisciplineApplicationsByslug = async (slug: string) => {
        const applicationRepository = getRepository(Application);
        return await applicationRepository
            .createQueryBuilder('application')
            .innerJoin("application.discipline", "discipline")
            .where('discipline.slug = :slug', {slug: slug})
            .getMany();
    };
}

export default ApplicationRepository;
