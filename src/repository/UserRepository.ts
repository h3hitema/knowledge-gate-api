import {getRepository} from "typeorm";
import {Discipline} from "../entity/Discipline";
import {Application} from "../entity/Application";
import {User} from "../entity/User";
import {ClassRoom} from "../entity/ClassRoom";

class UserRepository {
    static getUserById = async (id: number) => {
        const userRepository = getRepository(User);
        return await userRepository.findOneOrFail({
            where: [{id: id, deleted: false}]
        });
    };

    static getAllUsers = async () => {
        const userRepository = getRepository(User);
        return await userRepository.createQueryBuilder('user')
            .where("user.role != :role", {role: "DELETED"})
            .getMany()
    };

    static getUsersByIds = async (ids: number[]) => {
        const userRepository = getRepository(User);
        return await userRepository.createQueryBuilder('user')
            .where("id IN (:ids)", {ids: ids})
            .andWhere("deleted = :deleted", {deleted: false})
            .andWhere("role = :role", {role: "TEACHER"})
            .getMany()
    };

    static getUserByUsernameOrMail = async (auth: string) => {
        const userRepository = getRepository(User);
        return await userRepository.createQueryBuilder("user")
            .where("email = :email", {email: auth})
            .orWhere("username = :username", {username: auth})
            .getOne();
    };

    static getUserByEmail = async (email: string) => {
        const userRepository = getRepository(User);
        return await userRepository.findOneOrFail({
            where: [{email: email, deleted: false}]
        });
    };

    static getUserPassword = async (id: number) => {
        const userRepository = getRepository(User);
        return await userRepository.find({
            select: ["password"],
            where: [{id: id, deleted: false}]
        });
    };

    static getUserWithRelations = async (id: number) => {
        const userRepository = getRepository(User);
        return await userRepository.findOneOrFail({
            relations: ["teacher_disciplines", "student_disciplines", "marks", "applications"],
            where: [{id: id, deleted: false}]
        });
    };

    static findTeachersByIds = async (teachersId: number[]) => {
        const userRepository = getRepository(User);
        return await userRepository.findByIds(teachersId, {
            where: [{role: "TEACHER"}]
        });
    };


    static async getTeacherDisciplines(id: number) {
        const userRepository = getRepository(User);
        return await userRepository.findOneOrFail({
            relations: ["teacher_disciplines"],
            where: [{id: id}]
        });
    }

    static async getAllStudents() {
        const userRepository = getRepository(User);
        return await userRepository.findOneOrFail({
            where: [{role:"STUDENT"}]
        });
    }
}

export default UserRepository;
