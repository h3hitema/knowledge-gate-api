import {getRepository} from "typeorm";
import {Discipline} from "../entity/Discipline";
import {Application} from "../entity/Application";
import {User} from "../entity/User";

class StudentRepository {
    static getAll = async () => {
        const userRepository = getRepository(User);
        return await userRepository.find( {
            where: [{role: "STUDENT", deleted: false }]
        });
    };
}

export default StudentRepository;
