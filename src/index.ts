import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as helmet from "helmet";
import * as cors from "cors";
import routes from "./routes";
import {User} from "./entity/User";
import StudentRepository from "./repository/StudentRepository";
import DisciplineRepository from "./repository/DisciplineRepository";
import {Discipline} from "./entity/Discipline";
import ApplicationRepository from "./repository/ApplicationRepository";
import {Application} from "./entity/Application";

//Connects to the Database -> then starts the express
createConnection()
    .then(async connection => {
        // Create a new express application instance
        const app = express();

        // Call midlewares
        app.use(cors({origin: true}));
        app.use(helmet());
        app.use(bodyParser.urlencoded({extended: true}));

        app.use(bodyParser.json());

        //Set all routes from routes folder
        app.use("/", routes);
        app.listen(5000, () => {
            console.log("Server started on port 3000!");
        });
    })
    .catch(error => console.log(error));
