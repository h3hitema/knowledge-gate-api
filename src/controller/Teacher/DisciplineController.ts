import {Request, Response} from "express";
import {getRepository} from "typeorm";
import {validate} from "class-validator";
import {Exam} from "../../entity/Exam";
import {Discipline} from "../../entity/Discipline";
import DisciplineRepository from "../../repository/DisciplineRepository";
import {Application} from "../../entity/Application";
import ApplicationRepository from "../../repository/ApplicationRepository";
import UserRepository from "../../repository/UserRepository";
import {User} from "../../entity/User";
import {ClassRoom} from "../../entity/ClassRoom";


class DisciplineController {
    static getOneById = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id: number = Number(req.params.id);

        //Get the exam from database
        try {
            const discipline = await DisciplineRepository.getDisciplineWithRelations(id);
            console.log(discipline);

            res.send({discipline});
        } catch (error) {
            res.status(404).send("Exam not found");
        }
    };

    static createExam = async (req: Request, res: Response) => {
        //Get parameters from the body
        const discipline_id = req.params.id;

        let {title, date} = req.body;
        let exam = new Exam();
        exam.title = title;
        exam.date = date;
        exam.discipline = await getRepository(Discipline).findOneOrFail(discipline_id);

        //Validade if the parameters are ok
        const errors = await validate(exam);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        //Try to save. If fails, return errors
        const examRepository = getRepository(Exam);
        try {
            await examRepository.save(exam);
        } catch (e) {
            res.status(400).send(e.message);
            return;
        }

        //If all ok, send 201 response
        res.status(201).send("Exam created");
    };

    static edit = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id = req.params.id;

        //Get values from the body
        let {name, description} = req.body;

        //Try to find exam on database
        const examsRepository = getRepository(Exam);
        let exam;
        try {
            exam = await examsRepository.findOneOrFail(id);
        } catch (error) {
            //If not found, send a 404 response
            res.status(404).send("Exam not found");
            return;
        }

        //Validate the new values on model
        exam.name = name ? name : exam.name;
        exam.description = description ? description : exam.description;

        const errors = await validate(exam);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        //Try to save. If fails, the name is already in use
        const examRepository = getRepository(Exam);
        try {
            await examRepository.save(exam);
        } catch (e) {
            res.status(409).send(e.message);
            return;
        }

        //After all send a 204 (no content, but accepted) response
        //res.status(204).send();
        res.status(201).send({
            message: "Exam Updated",
            exam: exam
        });
    };

    static delete = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id = req.params.id;

        const examsRepository = getRepository(Exam);
        let exam: Exam;
        try {
            exam = await examsRepository.findOneOrFail(id);
        } catch (error) {
            res.status(404).send("Exam not found");
            return;
        }
        await examsRepository.delete(id);

        //After all send a 204 (no content, but accepted) response
        res.status(204).send();
    };

    static acceptStudent = async (req: Request, res: Response) => {
        //Get the ID from the url
        const {id} = req.params;
        const userRepository = getRepository(User);
        const applicationRepository = getRepository(Application);
        let application: Application;

        try {
            application = await ApplicationRepository.getApplicationWithRelations(Number(id));
        } catch (error) {
            res.status(404).send(error);
            return;
        }

        //const student = application.student;
        const student = await UserRepository.getUserWithRelations(application.student.id);
        const discipline = application.discipline;
        student.student_disciplines.push(discipline);

        try {
            await userRepository.save(student);
            await applicationRepository.delete(id);
        } catch (e) {
            res.status(404).send(e);
            return;
        }

        //After all send a 204 (no content, but accepted) response
        res.status(201).send({
            discipline: discipline,
            student: student,
            message: `${student.firstname} ${student.lastname} à été ajouter au cour de ${discipline.name}`
        });
    };

    static disciplineApplications = async (req: Request, res: Response) => {
        //Get the ID from the url
        const {slug} = req.params;
        const applicationRepository = getRepository(Application);
        let applications: Application [];

        try {
            applications = await ApplicationRepository.getDisciplineApplicationsByslug(slug)
        } catch (error) {
            res.status(404).send("Applications not found");
            return;
        }

        //After all send a 204 (no content, but accepted) response
        res.status(201).send(applications);
    };

    static teacherDisciplines = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id = req.params.id;
        let user: User;

        try {
            user = await UserRepository.getTeacherDisciplines(Number(id));
        } catch (error) {
            res.status(400).send(error);
            return;
        }

        //After all send a 204 (no content, but accepted) response
        res.status(200).send({disciplines: user.teacher_disciplines});
    }

    static createClassRoom = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id = req.params.id;
        let {dateStart, dateEnd} = req.body;
        let discipline: Discipline;

        let classroomRepository = getRepository(ClassRoom);
        try {
            discipline = await DisciplineRepository.getById(Number(id));
        } catch (error) {
            res.status(400).send(error);
            return;
        }
        let classroom = new ClassRoom();
        classroom.discipline = discipline;
        classroom.dateStart = dateStart * 1000;
        classroom.dateENd = dateEnd * 1000;

        if (classroom.dateStart > classroom.dateENd) {
            res.status(400).send({
                message: "La date de début ne peux etre inférieur à celle de fin"
            });
            return;
        }

        await classroomRepository.save(classroom);

        //After all send a 204 (no content, but accepted) response
        res.status(201).send({classroom});
    }
}

export default DisciplineController;