import {Request, Response} from "express";
import {getRepository} from "typeorm";
import {validate} from "class-validator";

import {User} from "../../entity/User";
import UserRepository from "../../repository/UserRepository";
import {Discipline} from "../../entity/Discipline";
import DisciplineRepository from "../../repository/DisciplineRepository";

class TeacherController {

    static listAll = async (req: Request, res: Response) => {
        //Get users from database
        const userRepository = getRepository(User);
        const teachers = await userRepository.find({
            where: [{role: "TEACHER"}]
        });

        //Send the users object
        res.send({teachers: teachers});
    };

    static addDisciplineTeachers = async (req: Request, res: Response) => {
        const {teacher_ids, discipline_id} = req.body;
        const disciplineRepository = getRepository(Discipline);
        let discipline: Discipline;
        let teachers: User[];
        try {
            discipline = await DisciplineRepository.getDisciplineWithTeachers(discipline_id);
            teachers = await UserRepository.getUsersByIds(teacher_ids);
            discipline.teachers.push(...teachers);
            await disciplineRepository.save(discipline);
        } catch (e) {
            res.status(404).send(e);
        }

        //Send the users object
        res.send({discipline: discipline});
    };

    static removeDisciplineTeachers = async (req: Request, res: Response) => {
        const {teacher_ids, discipline_id} = req.body;
        const disciplineRepository = getRepository(Discipline);
        let discipline: Discipline;
        let toDeleteteachers: User[];
        try {
            discipline = await DisciplineRepository.getDisciplineWithTeachers(discipline_id);
            toDeleteteachers = await UserRepository.getUsersByIds(teacher_ids);
            discipline.teachers = discipline.teachers.filter((teacher) => toDeleteteachers.indexOf(teacher) !== -1);
            await disciplineRepository.save(discipline);
        } catch (e) {
            res.status(404).send(e);
        }

        //Send the users object
        res.send({discipline: discipline});
    };

    static getOneById = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id: number = Number(req.params.id);
        //Get the user from database
        const userRepository = getRepository(User);
        try {
            const teacher = await userRepository.findOneOrFail(id, {
                where: [{role: "TEACHER"}]
            });
            res.send(teacher);
        } catch (error) {
            res.status(404).send("Teacher not found");
        }
    };

    static create = async (req: Request, res: Response) => {
        //Get parameters from the body
        let {username, password, firstname, lastname, email} = req.body;
        let teacher = new User();

        teacher.username = username;
        teacher.password = password;
        teacher.firstname = firstname;
        teacher.lastname = lastname;
        teacher.email = email;
        teacher.role = "TEACHER";

        //Validate if the parameters are ok
        const errors = await validate(teacher);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        //Hash the password, to securely store on DB
        teacher.hashPassword();

        //Try to save. If fails, the username is already in use
        const userRepository = getRepository(User);
        try {
            await userRepository.save(teacher);
        } catch (e) {
            res.status(409).send("username already in use");
            return;
        }

        //If all ok, send 201 response
        res.status(201).send("User created");
    };

    static edit = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id = req.params.id;

        //Get values from the body
        const {username, firstname, lastname, email} = req.body;

        //Try to find user on database
        const userRepository = getRepository(User);
        let teacher;
        try {
            teacher = await userRepository.findOneOrFail(id, {
                where: [{role: "TEACHER"}]
            });
        } catch (error) {
            //If not found, send a 404 response
            res.status(404).send("User not found");
            return;
        }

        //Validate the new values on model
        teacher.username = username ? username : teacher.username;
        teacher.firsname = firstname ? firstname : teacher.firsname;
        teacher.lastname = lastname ? lastname : teacher.lastname;
        teacher.email = email ? email : teacher.email;

        const errors = await validate(teacher);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        //Try to safe, if fails, that means username already in use
        try {
            await userRepository.save(teacher);
        } catch (e) {
            res.status(409).send("username already in use");
            return;
        }
        //After all send a 204 (no content, but accepted) response
        res.status(200).send({
            teacher: teacher,
            message: "Teacher Updated"
        });
    };
}

export default TeacherController;
