import {Request, Response} from "express";
import {getRepository} from "typeorm";
import {validate} from "class-validator";

import {Discipline} from "../../entity/Discipline";
import StringService from "../../service/StringService";
import DisciplineRepository from "../../repository/DisciplineRepository";
import UserRepository from "../../repository/UserRepository";
import {User} from "../../entity/User";

class DisciplineController {
    static listAll = async (req: Request, res: Response) => {
        //Get disciplines from database
        const disciplines = await DisciplineRepository.getAll();
        //Send the disciplines object
        res.send({disciplines});
    };

    static getOneById = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id: number = Number(req.params.id);

        try {
            const discipline = await DisciplineRepository.getDisciplineWithTeachers(id);
            res.send({discipline});
        } catch (error) {
            res.status(404).send(error);
        }
    };

    static create = async (req: Request, res: Response) => {
        //Get parameters from the body
        let {name, description} = req.body;
        let discipline = new Discipline();
        discipline.name = name;
        discipline.description = description;
        discipline.slug = StringService.slugify(name);

        //Validade if the parameters are ok
        const errors = await validate(discipline);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        //Try to save. If fails, the name is already in use
        const disciplineRepository = getRepository(Discipline);
        try {
            await disciplineRepository.save(discipline);
        } catch (e) {
            res.status(400).send(e.message);
            return;
        }

        //If all ok, send 201 response
        res.status(201).send({discipline});
    };

    static edit = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id = req.params.id;

        //Get values from the body
        let {name, description} = req.body;

        //Try to find discipline on database
        const disciplinesRepository = getRepository(Discipline);
        let discipline;
        try {
            discipline = await disciplinesRepository.findOneOrFail(id);
        } catch (error) {
            //If not found, send a 404 response
            res.status(404).send("Discipline not found");
            return;
        }

        //Validate the new values on model
        discipline.name = name ? name : discipline.name;
        discipline.description = description ? description : discipline.description;

        const errors = await validate(discipline);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        //Try to save. If fails, the name is already in use
        const disciplineRepository = getRepository(Discipline);
        try {
            await disciplineRepository.save(discipline);
        } catch (e) {
            res.status(409).send(e.message);
            return;
        }

        //After all send a 204 (no content, but accepted) response
        //res.status(204).send();
        res.status(201).send({
            message: "Discipline Updated",
            discipline: discipline
        });
    };

    static delete = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id = req.params.id;

        const disciplinesRepository = getRepository(Discipline);
        let discipline: Discipline;

        try {
            discipline = await disciplinesRepository.findOneOrFail(id);
            await disciplinesRepository.delete(id);
        } catch (error) {
            res.status(400).send(error);
            return;
        }

        res.status(202).send({
            discipline
        });
    };

    static editTeachers = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id = req.params.id;
        //Get values from the body
        let {teachersId} = req.body;

        //Try to find discipline on database
        const disciplinesRepository = getRepository(Discipline);
        let discipline: Discipline;
        let teachers: User[];
        try {
            discipline = await disciplinesRepository.findOneOrFail(id);
            teachers = await UserRepository.findTeachersByIds(teachersId);
        } catch (error) {
            //If not found, send a 404 response
            res.status(400).send(error);
            return;
        }
        //Validate the new values on model
        discipline.teachers = teachers;

        const errors = await validate(discipline);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        //Try to save. If fails, the name is already in use
        const disciplineRepository = getRepository(Discipline);
        try {
            await disciplineRepository.save(discipline);
        } catch (e) {
            res.status(409).send(e.message);
            return;
        }

        //After all send a 204 (no content, but accepted) response
        //res.status(204).send();
        res.status(201).send({
            message: "Discipline Updated",
            discipline: discipline
        });
    };

}

export default DisciplineController;
