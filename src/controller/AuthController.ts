import {Request, Response} from "express";
import * as jwt from "jsonwebtoken";
import {getRepository} from "typeorm";
import {validate} from "class-validator";

import {User} from "../entity/User";
import config from "../config/config";
import UserRepository from "../repository/UserRepository";

class AuthController {
    static login = async (req: Request, res: Response) => {
        //Check if username and password are set
        let {username, password} = req.body;
        if (!(username && password)) {
            res.status(400).send();
        }

        //Get user from database
        let user: User;
        try {
            user = await UserRepository.getUserByUsernameOrMail(username);
        } catch (error) {
            res.status(401).send();
        }

        //Check if encrypted password match
        if (!user.checkIfUnencryptedPasswordIsValid(password)) {
            res.status(401).send();
            return;
        }

        //Sing JWT, valid for 1 hour
        const token = jwt.sign(
            {userId: user.id, username: user.username},
            config.jwtSecret);

        let body = {
            token: token,
            user: user
        };

        //Send the jwt in the response
        return res.status(200).json(body);
    };

    static changePassword = async (req: Request, res: Response) => {
        //Get ID from JWT
        const id = res.locals.jwtPayload.userId;

        //Get parameters from the body
        const {oldPassword, newPassword} = req.body;
        if (!(oldPassword && newPassword)) {
            res.status(400).send();
        }

        //Get user from the database
        const userRepository = getRepository(User);
        let user: User;
        try {
            user = await userRepository.findOneOrFail(id);
        } catch (id) {
            res.status(401).send();
        }

        //Check if old password matchs
        if (!user.checkIfUnencryptedPasswordIsValid(oldPassword)) {
            res.status(401).send();
            return;
        }

        //Validate de model (password lenght)
        user.password = newPassword;
        const errors = await validate(user);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }
        //Hash the new password and save
        user.hashPassword();
        await userRepository.save(user);

        res.status(204).send();
    };
}

export default AuthController;
