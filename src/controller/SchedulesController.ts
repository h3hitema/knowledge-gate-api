import {Request, Response} from "express";
import {getRepository} from "typeorm";
import {validate} from "class-validator";

import {User} from "../entity/User";
import {Discipline} from "../entity/Discipline";
import UserRepository from "../repository/UserRepository";

class SchedulesController {
    static listDisciplineSchedules = async (req: Request, res: Response) => {
        //Get users from database
        let {disciplineId} = req.body;
        const discipline = getRepository(Discipline);
        const shedules = await discipline
            .createQueryBuilder("user")
            .innerJoin("user.classrooms","classroom")
            .innerJoin('classroom.dateInterval','dateInterval')
            .where("discipline.id = :id", {id : disciplineId })
            .select(["dateInterval"])
            .getMany()
        ;
        //Send the users object
        res.send(shedules);
    };

    static getOneById = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id: number = Number(req.params.id);

        //Get the user from database
        const userRepository = getRepository(User);
        try {
            const user = await UserRepository.getUserById(id);
            res.send(user);
        } catch (error) {
            res.status(404).send("User not found");
        }
    };

}

export default SchedulesController;
