import {Request, Response} from "express";
import {getRepository} from "typeorm";
import {validate} from "class-validator";

import {User} from "../../entity/User";
import {Discipline} from "../../entity/Discipline";
import {Application} from "../../entity/Application";
import DisciplineRepository from "../../repository/DisciplineRepository";
import ApplicationRepository from "../../repository/ApplicationRepository";

class StudentController {

    static register = async (req: Request, res: Response) => {
        //Get parameters from the body
        let {username, password, firstname, lastname, email} = req.body;
        let user = new User();
        user.username = username;
        user.firstname = firstname;
        user.lastname = lastname;
        user.email = email;
        user.password = password;
        user.role = "STUDENT";

        //Validade if the parameters are ok
        const errors = await validate(user);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        //Hash the password, to securely store on DB
        user.hashPassword();

        const userRepository = getRepository(User);
        try {
            await userRepository.save(user);
        } catch (e) {
            res.status(409).send("username already in use");
            return;
        }

        //If all ok, send 201 response
        res.status(201).send("User created");
    };

    static edit = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id = req.params.id;
        const {firstname, lastname, email} = req.body;
        const userRepository = getRepository(User);
        let user;

        try {
            user = await userRepository.findOneOrFail(id);
        } catch (error) {
            res.status(404).send("User not found");
            return;
        }

        //Validate the new values on model
        user.firstname = firstname;
        user.lastname = lastname;
        user.email = email;

        const errors = await validate(user);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        //Try to safe, if fails, that means username already in use
        try {
            await userRepository.save(user);
        } catch (e) {
            res.status(409).send("username already in use");
            return;
        }
        //After all send a 204 (no content, but accepted) response
        res.status(204).send();
    };

    static apply = async (req: Request, res: Response) => {
        //Get the ID from the url
        const discipline_id = req.params.id;
        const {student_id, message} = req.body;
        const userRepository = getRepository(User);
        const disciplineRepository = getRepository(Discipline);
        const applicationRepository = getRepository(Application);
        let student : User;
        let discipline : Discipline;
        let application : Application;

        try {
            student = await userRepository.findOneOrFail(student_id);
            discipline = await disciplineRepository.findOneOrFail(discipline_id);
        } catch (error) {
            res.status(404).send("User not found");
            return;
        }

        application = new Application();
        application.student =  student;
        application.discipline = discipline;
        application.message = message;


        const errors = await validate(application);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        //Try to safe, if fails, that means username already in use
        try {
            await applicationRepository.save(application);
        } catch (e) {
            res.status(409).send({
                message: e.message,
                code: e.errno
            });
            return;
        }
        //After all send a 204 (no content, but accepted) response
        res.status(204).send({
            message: "The Application was succesfuly created"
        });
    };
}

export default StudentController;
