import {getRepository, MigrationInterface, QueryRunner} from "typeorm";
import StudentRepository from "../repository/StudentRepository";
import {User} from "../entity/User";
import {Discipline} from "../entity/Discipline";
import DisciplineRepository from "../repository/DisciplineRepository";
import {Application} from "../entity/Application";
import faker from "faker";
export class CreateStudentsApplications1582700703695 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const students: User[]= await StudentRepository.getAll();
        const disciplines: Discipline[]= await DisciplineRepository.getAll();
        const applicationRepository = getRepository(Application);
        const faker = require("faker");

        students.forEach((student: User) => {
            disciplines.forEach((discipline: Discipline) => {
                let application = new Application();
                application.student = student;
                application.discipline = discipline;
                application.message = faker.lorem.text();
                applicationRepository.save(application);
            })
        })

    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
