import {getRepository, MigrationInterface, QueryRunner} from "typeorm";
import {Discipline} from "../entity/Discipline";
import {User} from "../entity/User";
import StringService from "../service/StringService";

export class CreateDisciplines1582485362431 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        let disciplines = {
            Anglais: "Cours d'anglais",
            Math: "Cours d'anglais",
            Espagnol: "Cours d'anglais",
            Histoire: "Cours d'anglais",
            SVT: "Cours de SVT",
            Geographie: "Cours de géographie",
            "Science Physique": "Cours d'anglais",
        }

        for (let key in disciplines) {
            let discipline = new Discipline();
            discipline.name = key;
            discipline.description = disciplines[key];
            discipline.slug = StringService.slugify(key);
            const disciplineRepository = getRepository(Discipline);
            await disciplineRepository.save(discipline);
        }
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
