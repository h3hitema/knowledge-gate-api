import {getRepository, MigrationInterface, QueryRunner} from "typeorm";
import StringService from "../service/StringService";
import {User} from "../entity/User";


export class CreateStudents1582699638348 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const faker = require("faker");
        faker.locale = "fr";
        const userRepository = getRepository(User);

        for (let i =0; i< 20; i++ ) {
            let student = new User();
            student.username = faker.internet.userName();
            student.firstname = faker.name.firstName();
            student.lastname = faker.name.lastName();
            student.email = faker.internet.email();
            student.password = "student";
            student.hashPassword();
            student.role = "STUDENT";
            await userRepository.save(student);
        }
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
