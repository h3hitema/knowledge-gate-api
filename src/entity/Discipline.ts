import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable, OneToMany, Unique} from "typeorm";
import { User } from "./User";
import { ClassRoom } from "./ClassRoom";
import { Exam } from "./Exam";
import { Notification } from "./Notification";
import {Application} from "./Application";

@Entity()
@Unique(["name"])
export class Discipline {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    slug: string;

    @Column()
    description: string;

    @ManyToMany(type => User, user => user.student_disciplines , {
        onDelete : "CASCADE",
    })
    @JoinTable({name: 'discipline_student'})
    students: User[];

    @ManyToMany(type => User, user => user.teacher_disciplines , {
        onDelete : "CASCADE",
        onUpdate: "CASCADE",
        cascade:true,
    })
    @JoinTable({name: 'discipline_teacher'})
    teachers: User[];

    @OneToMany(type => Exam, exam => exam.discipline , {
        onDelete : "CASCADE"
    })
    exams: Exam[];

    @OneToMany(type => ClassRoom, classroom => classroom.discipline, {
        onDelete : "CASCADE",
        eager: true
    })
    classrooms: ClassRoom[];

    @OneToMany(type => Notification, notification => notification.discipline, {
        onDelete : "CASCADE"
    })
    notifications: Notification[];

    @OneToMany(type => Application, application => application.discipline, {
        onDelete : "CASCADE",
        eager:true
    })
    applications: Application[];
}