import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    Unique,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToMany,
    OneToMany
} from "typeorm";
import {Length, IsNotEmpty} from "class-validator";
import * as bcrypt from "bcryptjs";
import {Discipline} from "./Discipline";
import {Mark} from "./Mark";
import {Application} from "./Application";

@Entity()
@Unique(["username"])
export class User {
    constructor() {
        this.deleted = false;
    }

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @Length(4, 20)
    username: string;

    @Column()
    firstname: string;

    @Column()
    lastname: string;

    @Column()
    email: string;

    @Column({select: false})
    deleted: boolean;

    @Column()
    @Length(4, 100)
    password: string;

    @Column()
    @IsNotEmpty()
    role: string;

    @ManyToMany(
        type => Discipline,
        discipline => discipline.teachers)
    teacher_disciplines: Discipline[];

    @ManyToMany(type => Discipline, discipline => discipline.students)
    student_disciplines: Discipline[];

    @OneToMany(type => Mark, mark => mark.student)
    marks: Mark[];

    @OneToMany(type => Application, application => application.student)
    applications: Application[];

    hashPassword() {
        this.password = bcrypt.hashSync(this.password, 8);
    }

    checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
        return bcrypt.compareSync(unencryptedPassword, this.password);
    }
}
