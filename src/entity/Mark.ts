import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import { Exam } from "./Exam";
import { type } from "os";
import { User } from "./User";

@Entity()
export class Mark {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    mark: number;

    // remarque
    @Column()
    note: string;

    @ManyToOne(type => Exam, exam => exam.marks)
    exam: Exam;

    @ManyToOne(type => User, user => user.marks)
    student: User
}