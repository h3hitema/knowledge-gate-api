import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    OneToMany,
    CreateDateColumn,
    JoinColumn,
    Unique
} from "typeorm";
import { Discipline } from "./Discipline";
import { type } from "os";
import { Mark } from "./Mark";
import {User} from "./User";

@Entity()
@Unique("UQ_Application", ["discipline", "student"])
export class Application {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    message: string;

    @Column()
    @CreateDateColumn()
    date: Date;

    @ManyToOne(type => Discipline, discipline => discipline.applications)
    @JoinColumn({ name: "discipline_id" })
    discipline: Discipline;

    @ManyToOne(type => User, user => user.applications, {
        eager:true
    })
    @JoinColumn({ name: "student_id" })
    student: User;
}