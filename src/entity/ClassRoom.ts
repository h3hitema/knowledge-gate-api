import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, CreateDateColumn, OneToMany} from 'typeorm';
import {Discipline} from "./Discipline";

@Entity({name: 'classroom'})
export class ClassRoom {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    dateStart: number;

    @Column()
    dateENd: number;

    @ManyToOne(type => Discipline, discipline => discipline.classrooms)
    discipline: Discipline;
}