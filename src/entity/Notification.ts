import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, CreateDateColumn} from "typeorm";
import { Discipline } from "./Discipline";

@Entity()
export class Notification {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @CreateDateColumn()
    dateStart: Date;

    @Column()
    @CreateDateColumn()
    dateEnd: Date;

    @Column()
    title: string;

    @Column()
    description: string;

    @ManyToOne(type => Discipline, discipline => discipline.notifications)
    discipline: Discipline;
}