import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, CreateDateColumn} from "typeorm";
import { Discipline } from "./Discipline";
import { type } from "os";
import { Mark } from "./Mark";

@Entity()
export class Exam {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    @CreateDateColumn()
    date: Date;

    @ManyToOne(type => Discipline, discipline => discipline.exams)
    discipline: Discipline;

    @OneToMany(type => Mark, mark => mark.exam)
    marks: Exam;
}