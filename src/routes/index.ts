import { Router, Request, Response } from "express";
import auth from "./auth";
import admin from "./admin/admin";
import student from "./student/student";
import teacher from "./teacher/teacher";

const routes = Router();

//Routes d'authentification
routes.use("/auth", auth);
// Routes admin
routes.use("/admin", admin);
// Routes Teachers
routes.use("/teacher", teacher);
// Routes Students
routes.use("/student", student);

export default routes;
