import { Router, Request, Response } from "express";
import user from "./user";
import discipline from "./discipline"
import teacher from "./teacher"

const routes = Router();
// Routes /admin/user
routes.use("/user", user);
// Routes /discipline/user
routes.use("/discipline", discipline);
// Routes /teacher/user
routes.use("/teacher", teacher);

export default routes;
