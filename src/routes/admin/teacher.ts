import { Router } from "express";
  import UserController from "../../controller/Admin/UserController";
  import { checkJwt } from "../../middlewares/checkJwt";
  import { checkRole } from "../../middlewares/checkRole";
import TeacherController from "../../controller/Admin/TeacherController";

  const router = Router();

  //Get all Teachers
  router.get("/", [checkJwt, checkRole(["ADMIN"])], TeacherController.listAll);

  // Get one Teacher
  router.get(
    "/:id([0-9]+)",
    [checkJwt, checkRole(["ADMIN"])],
    TeacherController.getOneById
  );

  //Create a new Teacher
  router.post("/", [checkJwt, checkRole(["ADMIN"])], TeacherController.create);

  //Edit one Teacher
  router.patch(
    "/:id([0-9]+)",
    [checkJwt, checkRole(["ADMIN"])],
    TeacherController.edit
  );

  export default router;