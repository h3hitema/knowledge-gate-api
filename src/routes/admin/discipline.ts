import { Router } from "express";
  import UserController from "../../controller/Admin/UserController";
  import { checkJwt } from "../../middlewares/checkJwt";
  import { checkRole } from "../../middlewares/checkRole";
import DisciplineController from "../../controller/Admin/DisciplineController";

  const router = Router();

  //Get all Disciplines
  router.get("/", [checkJwt, checkRole(["ADMIN"])], DisciplineController.listAll);

  // Get one Discipline
  router.get(
    "/:id([0-9]+)",
    [checkJwt, checkRole(["ADMIN"])],
    DisciplineController.getOneById
  );

  //Create a new Discipline
  router.post("/", [checkJwt, checkRole(["ADMIN"])], DisciplineController.create);

  //Edit one Discipline
  router.patch(
    "/:id([0-9]+)",
    [checkJwt, checkRole(["ADMIN"])],
    DisciplineController.edit
  );

  //Update Discipline Teachers
  router.post(
    "/:id([0-9]+)/teachers",
    [checkJwt, checkRole(["ADMIN"])],
    DisciplineController.editTeachers
  );

  //Delete one Discipline
  router.delete(
    "/:id([0-9]+)",
    [checkJwt, checkRole(["ADMIN"])],
    DisciplineController.delete
  );

  export default router;