import { Router } from "express";
import UserController from "../../controller/Admin/UserController";
import { checkJwt } from "../../middlewares/checkJwt";
import { checkRole } from "../../middlewares/checkRole";
import TeacherController from "../../controller/Admin/TeacherController";

const router = Router();


export default router;