import { Router } from "express";
import UserController from "../../controller/Admin/UserController";
import { checkJwt } from "../../middlewares/checkJwt";
import { checkRole } from "../../middlewares/checkRole";
import TeacherController from "../../controller/Admin/TeacherController";
import DisciplineController from "../../controller/Teacher/DisciplineController";

const router = Router();

// Accept an application to a discipline
router.post(
    "/apply/accept/:id",
    [checkJwt, checkRole(["TEACHER"])],
    DisciplineController.acceptStudent
);

// get Applications
router.get(
    "/applications",
    [checkJwt, checkRole(["TEACHER"])],
    DisciplineController.disciplineApplications
);

// Get Discipline
router.get(
    "/:id([0-9]+)",
    [checkJwt, checkRole(["TEACHER"])],
    DisciplineController.getOneById
);

// Create Discipline ClassRoomes
router.post(
    "/:id([0-9]+)/classroom",
    [checkJwt, checkRole(["TEACHER"])],
    DisciplineController.createClassRoom
);

export default router;