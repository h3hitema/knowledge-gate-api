import { Router, Request, Response } from "express";
import classroom from "./classroom"
import exam from "./exam"
import mark from "./mark"
import discipline from "./discipline"
import {checkJwt} from "../../middlewares/checkJwt";
import {checkRole} from "../../middlewares/checkRole";
import DisciplineController from "../../controller/Teacher/DisciplineController";
import UserController from "../../controller/Admin/UserController";

const routes = Router();

routes.use("/exam", exam);
routes.use("/mark", mark);
routes.use("/classroom", classroom);
routes.use("/discipline", discipline);

// Get teacher disciplines
routes.get("/:id([0-9]+)/disciplines", [checkJwt, checkRole(["TEACHER"])],
    DisciplineController.teacherDisciplines);

// Get all Students
routes.get("/students", [checkJwt, checkRole(["TEACHER"])],
    UserController.listStudents);

export default routes;
