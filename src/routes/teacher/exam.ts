import { Router } from "express";
import UserController from "../../controller/Admin/UserController";
import { checkJwt } from "../../middlewares/checkJwt";
import { checkRole } from "../../middlewares/checkRole";
import TeacherController from "../../controller/Admin/TeacherController";

const router = Router();

// //Get all users
// router.get("/", [checkJwt, checkRole(["TEACHER"])], TeacherController.listAll);
//
// // Get one user
// router.get(
//     "/:id([0-9]+)",
//     [checkJwt, checkRole(["TEACHER"])],
//     TeacherController.getOneById
// );
//
// //Create a new user
// router.post("/", [checkJwt, checkRole(["TEACHER"])], TeacherController.create);
//
// //Edit one user
// router.patch(
//     "/:id([0-9]+)",
//     [checkJwt, checkRole(["TEACHER"])],
//     TeacherController.edit
// );

//Delete one user
// router.delete(
//     "/:id([0-9]+)",
//     [checkJwt, checkRole(["TEACHER"])],
//     TeacherController.delete
// );

export default router;