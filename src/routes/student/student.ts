import { Router } from "express";
import discipline from "./discipline";
import user from "./discipline";
import StudentController from "../../controller/Student/StudentController";

const routes = Router();

routes.use("/discipline", discipline);
routes.post("/register", StudentController.register);

export default routes;
