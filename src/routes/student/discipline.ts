import { Router } from "express";
import UserController from "../../controller/Admin/UserController";
import { checkJwt } from "../../middlewares/checkJwt";
import { checkRole } from "../../middlewares/checkRole";
import DisciplineController from "../../controller/Admin/DisciplineController";
import StudentController from "../../controller/Student/StudentController";
import DisciplineRepository from "../../repository/DisciplineRepository";

const router = Router();

//Get Disciplines
router.get("/", [checkJwt, checkRole(["STUDENT"])], DisciplineController.listAll);


//Get one Disciplines
router.get(
    "/:id([0-9]+)",
    [checkJwt, checkRole(["STUDENT"])],
    DisciplineController.getOneById
);

//Apply on one Discipline
router.post("/:id([0-9]+)/apply", [checkJwt, checkRole(["STUDENT"])], StudentController.apply);

export default router;